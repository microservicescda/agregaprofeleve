package com.eleveagrega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EleveAgregaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EleveAgregaApplication.class, args);
    }

}
