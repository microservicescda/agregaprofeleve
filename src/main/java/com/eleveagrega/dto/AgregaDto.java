package com.eleveagrega.dto;

import java.util.List;

public class AgregaDto {
    ClasseDtoOut classe;
    List<EleveDtoOut> eleves;
}
