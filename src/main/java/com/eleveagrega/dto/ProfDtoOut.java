package com.eleveagrega.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfDtoOut {
    ProfDto prof;
    List<ClasseDtoOut> classes;
}

class ClasseDtoOut {
    String id;
    String nom;
}


