package com.eleveagrega.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfDtoIn {
    ProfDto prof;
    List<ClasseDto> classes;

}

class ClasseDto {
    String id;
    String nom;
    List<String> eleves;
}

class ProfDto {
    String id;
    String nom;
    String prenom;
    String matieres;
}
