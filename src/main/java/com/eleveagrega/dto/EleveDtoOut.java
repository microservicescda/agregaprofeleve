package com.eleveagrega.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EleveDtoOut {
    String _id;
    String nom;
    String prenom;
}
